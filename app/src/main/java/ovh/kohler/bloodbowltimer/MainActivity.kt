package ovh.kohler.bloodbowltimer

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.WindowManager
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.activity.ComponentActivity
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat


class MainActivity : ComponentActivity() {
    var myWebView : WebView? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()

        WindowCompat.setDecorFitsSystemWindows(window, false)
        this@MainActivity.actionBar?.hide()

        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Check sdk version
        window.attributes.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES

        val windowInsetsController =
            WindowCompat.getInsetsController(window, window.decorView)
        // Configure the behavior of the hidden system bars.
        windowInsetsController.systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())

        this.myWebView = WebView(this@MainActivity)
        setContentView(this.myWebView)
        this.myWebView?.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.bbred))

        this.myWebView?.setSoundEffectsEnabled(true)
        this.myWebView?.settings?.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
        this.myWebView?.settings?.useWideViewPort = true
        this.myWebView?.settings?.javaScriptEnabled = true
        this.myWebView?.settings?.mediaPlaybackRequiresUserGesture = false
        this.myWebView?.loadUrl("https://bbtimer.kohler.ovh/")
    }

    override fun onStart() {
        super.onStart()
        this.myWebView?.evaluateJavascript("document.dispatchEvent(new Event('onMobileAppResume'));", null)
    }

    override fun onStop() {
        super.onStop()
        this.myWebView?.evaluateJavascript("document.dispatchEvent(new Event('onMobileAppQuit'));", null)
    }
}